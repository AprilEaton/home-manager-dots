{ config, pkgs, ... }:

{
  imports = [
    ./theme.nix
    ./rofi.nix
    ./env.nix
    ./zsh.nix
    ./mpv.nix
  ];

  nixpkgs.config.allowUnfree = true;
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "april";
  home.homeDirectory = "/home/april";

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "24.05"; # Please read the comment before changing.

  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = with pkgs; [
    emacs
    alacritty
    # rofi # I'm sorry, why is this allowed to work? Why does enabling something in home-manager *sometimes* cause package collisions?
    ripgrep
    discord
    nitrogen
    cmake
    clang
    libvterm
    rustc
    cargo
    rustfmt
    rust-analyzer
    clippy
    onlyoffice-bin
    anki
    brave

    # Game shit
    prismlauncher
    steam

    # Some good cli tools I use
    eza
    bat
    zip
    unzip # WHY IS THIS NOT JUST IN THE ZIP PACKAGE‽

    # Fonts, fonts, fonts galore!
    fira-code-nerdfont
    fira-code
  ];

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    # # Building this configuration will create a copy of 'dotfiles/screenrc' in
    # # the Nix store. Activating the configuration will then make '~/.screenrc' a
    # # symlink to the Nix store copy.
    # ".screenrc".source = dotfiles/screenrc;

    # # You can also set the file content immediately.
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';
  };

  catppuccin = {
    enable = true;
    accent = "mauve";
    flavor = "macchiato";
    pointerCursor.enable = true;
    pointerCursor.accent = "lavender";
    pointerCursor.flavor = "macchiato";
  };

  i18n.inputMethod.fcitx5.catppuccin = {
    apply = true;
    flavor = "macchiato";
  };

  programs.zsh.enable = true;
  programs.bash.enable = true;

  programs.alacritty.enable = true;
  programs.firefox.enable = true;

  home.sessionPath = [
    "~/.config/emacs/bin"
  ];

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
