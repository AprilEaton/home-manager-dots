{ config, lib, pkgs, ... }:

{
  home.sessionVariables = {
    EDITOR = "emacsclient";
  };
}
