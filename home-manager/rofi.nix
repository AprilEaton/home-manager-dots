{ config, lib, pkgs, ... }:

{
  programs.rofi = {
    enable = true;

    plugins = with pkgs; [
      rofi-calc
      rofi-top
      rofi-power-menu
      rofi-emoji
    ];

    location = "bottom-left";
  };
}
