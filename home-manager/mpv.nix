{ config, lib, pkgs, ... }:

{
  config.programs.mpv = {
    enable = true;

    package = (
      pkgs.mpv-unwrapped.wrapper {
        scripts = with pkgs.mpvScripts; [
          uosc
          sponsorblock
          mpvacious
        ];

        mpv = pkgs.mpv-unwrapped;
      }
    );
  };

  config = {
  };
}
